# coding=utf-8
import requests
import json


class GPlayPermissionFetcher:
    @staticmethod
    def get_permissions(app_id, lang='en'):
        headers = {
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) '
                          'Chrome/42.0.2311.90 Safari/537.36'}
        params = {
            'ids': app_id,
            'hl': lang,
            'xhr': 1
        }
        page = requests.post('https://play.google.com/store/xhr/getdoc?authuser=0', data=params, headers=headers)
        page_response = page.text[5:].replace(',,', ',null,').replace(',,', ',null,').replace('\[,', '[null,')
        jsono = json.loads(page_response)
        return jsono[0][2][0][65]['42656262'][1] or []
