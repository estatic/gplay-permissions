# coding=utf-8
from gplay import Application, Permission
from gplay.service.google.parser import GPlayPermissionFetcher


class PermissionManager:

    def get_permissions(self, app_id=None, lang='en'):
        app = self._load_permissions(app_id, lang)
        grouped = {}
        for p in app.permissions:
            if p.group not in grouped:
                grouped[p.group] = []
            grouped[p.group].append((p.short, p.description))
        data = {
            'appId': app.appId,
            'permissions': grouped
        }
        return data

    def _load_permissions(self, app_id, lang):
        assert app_id is not None
        app = self.__get_application_with_permissions(app_id, lang)
        if app is not None:
            return app

        raw_data = GPlayPermissionFetcher.get_permissions(app_id, lang=lang)
        permissions = self.__parse_permissions(raw_data)
        return self.__save_in_database(app_id, permissions, lang)

    @staticmethod
    def __get_application_with_permissions(app_id, lang):
        app = Application.query.filter_by(appId=app_id, lang=lang).first()
        return app

    @staticmethod
    def __parse_permissions(raw_data):
        permissions = []
        for root in raw_data:
            for subroot in root:

                latest = subroot[-1]
                group = 999
                if isinstance(latest, int):
                    group = latest
                print("GROUP: {}".format(group))
                for ssub in subroot:
                    if isinstance(ssub, list):

                        for perm in ssub:
                            permissions.append([perm[0], perm[1], group])

        return permissions

    @staticmethod
    def __save_in_database(app_id, permissions, lang):
        from gplay import db
        app = Application(app_id, app_id, lang)
        for perm in permissions:
            permission = Permission()
            permission.short = perm[0]
            permission.description = perm[1]
            permission.group = perm[2]
            app.permissions.append(permission)
        db.session.add(app)
        db.session.commit()
        return app
