# coding=utf-8
from gplay.service.google.parser import GPlayPermissionFetcher
from gplay.service.manager import PermissionManager


def test_fetch_permissions():
    permissions = GPlayPermissionFetcher.get_permissions('org.telegram.messenger')
    perms = []
    for root in permissions:
        for subroot in root:
            for ssub in subroot:
                if isinstance(ssub, list):
                    for perm in ssub:
                        perms.append([perm[0], perm[1]])


def test_fetch_with_manager():
    manager = PermissionManager()
    manager.get_permissions('org.telegram.messenger')


if __name__ == '__main__':
    test_fetch_permissions()
    test_fetch_with_manager()
