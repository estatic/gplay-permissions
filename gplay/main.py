#!/usr/bin/env python3
# coding=utf-8


from flask import render_template
from flask import request, send_from_directory
from flask_accept import accept
from gplay import app


@app.route('/', methods=['GET', 'POST'])
@accept('text/html')
def get_index():
    return render_template('index.html')


@app.route('/search', methods=['POST'])
def get_permissions():
    from gplay.service.manager import PermissionManager
    appId = request.form["appId"]
    lang = request.form["lang"] or 'en'
    manager = PermissionManager()
    return render_template('permissions.html', app=manager.get_permissions(appId, lang=lang))

@app.route('/fonts/<path:path>')
def send_js(path):
    return send_from_directory('bower_components/semantic-ui/build/packaged/fonts', path)

if __name__ == '__main__':
    app.run(debug=True, use_reloader=True)
