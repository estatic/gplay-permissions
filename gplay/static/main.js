$( document ).ready(function() {

    $('#load-permissions-button').click(function() {
        var appId = $("#query-field").val();
        var lang = $("#lang").val();
        $.post( "/search", {"appId": appId, "lang": lang}, function( data ) {
            $("#loader").addClass("active");
        }).done(function(data){
            $("#results").html(data);
        }).always(function() {
        $("#loader").removeClass("active");
      });
    });
});

