# coding=utf-8
from flask import Flask
from flask_restful import Api

from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager
from flask_assets import Environment, Bundle

import os

app = Flask(__name__)
api = Api(app)
# Setup Flask Assets
env = Environment(app)

# Define asset locations for search
env.load_path = [
    os.path.join(os.path.dirname(__file__), 'sass'),
    os.path.join(os.path.dirname(__file__), 'static'),
    os.path.join(os.path.dirname(__file__), 'bower_components'),

]

# Register javascripts and coffeescript
env.register(
    'js_all',
    Bundle(
        'jquery/dist/jquery.min.js',
        'semantic-ui/build/packaged/javascript/semantic.min.js',
        'main.js',
        output='js_all.js'
    )
)

# Register SASS and CSS
env.register(
    'css_all',
    Bundle(
        'semantic-ui/build/packaged/css/semantic.css',
        'icons.css',
        output='semantic_all.css'
    ),
)
env.init_app(app)

app.config.from_object(os.environ['APP_SETTINGS'])
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
migrate = Migrate(app, db)

manager = Manager(app)
manager.add_command('db', MigrateCommand)

from sqlalchemy.orm import relationship


class Application(db.Model):
    __tablename__ = 'applications'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(255))
    appId = db.Column(db.String(255))
    lang = db.Column(db.String(3))
    permissions = relationship("Permission", backref="application")

    def __init__(self, title, appId, lang):
        self.title = title
        self.appId = appId
        self.lang = lang

    def __repr__(self):
        return '<Application title:{} appId:{}>'.format(self.title, self.appId)


class Permission(db.Model):
    __tablename__ = 'permissions'
    id = db.Column(db.Integer, primary_key=True)
    short = db.Column(db.String(150))
    description = db.Column(db.String(250))
    group = db.Column(db.Integer())
    appId = db.Column(db.Integer(), db.ForeignKey('applications.id'))

    def __repr__(self):
        return '<Permission {}({})>'.format(self.short, self.description)
