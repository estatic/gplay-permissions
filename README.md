# How to install

1. Required packages

- Python3
- Any database SQLAlchemy supports (just add dependency to required database driver)
- database itself with access

2. Install dependencies

Just run
```bash
$ pip install .
```

3. Initialize database

First, you must provide environ variable DATABASE_URL with database credentials like:
```bash
export DATABASE_URL=mysql+pymysql://user:password@server:port/database
```

You can export this variable or change the variable value in .env file 

To initialize database you need to run commands:
```bash
$ ./db_upgrade.sh
```

4. Run application

You can run it with few ways:
```bash
$ ./start.sh
```

```bash
$ heroku local
```

```bash
$ foreman start
```