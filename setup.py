# coding=utf-8
import os
from setuptools import setup
from pip.req import parse_requirements
from pip.download import PipSession

# parse_requirements() returns generator of pip.req.InstallRequirement objects
install_reqs = parse_requirements(os.path.join(os.curdir, "requirements.txt"), session=PipSession())

# reqs is a list of requirement
reqs = [str(ir.req) for ir in install_reqs]

setup(
    name='gplay',
    packages=['gplay'],
    include_package_data=True,
    install_requires=reqs
)
